== Basic setup

=== Bootstrap

I have used the bootstrap simple sidebar.

To use bootstrap inside this application I chose to use webjars. With this dependency maven will pick up the bootstrap version and place it in your project.

[source,xml]
----
<dependency>
    <groupId>org.webjars</groupId>
    <artifactId>bootstrap</artifactId>
    <version>5.1.0</version>
</dependency>
----

=== Instant feedback

To serve the templates from src/main/resources instead of the output directory youneed to create a application-dev.yml file and add the following:

[source,yaml]
----
spring:
  thymeleaf:
    prefix: file:src/main/resources/templates/
    cache: false

  web:
    resources:
      static-locations: file:src/main/resources/static
      cache:
        period: 0
----

And make sure the dev profile is loaded by adding the following to your application.yml:

[source,yaml]
----
spring:
    profiles:
      active: dev
----

=== Translation

Default the translations will be found in messages files in the resources directory.

To change this to a subdirectory (translation) you put in the following:

[source,yaml]
----
spring:
  messages:
    basename: translation/messages
----

=== Database

I have added a docker-compose.yaml file to start a postgres database

[source,yaml]
----
version: '3'
services:
  db:
    image: 'postgres:12'
    ports:
      - "5432:5432"
    environment:
      POSTGRES_DB: ${POSTGRES_DATABASE}
      POSTGRES_USER: ${POSTGRES_USER}
      POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
----

The credentials are found in the .env file

[source,text]
----
POSTGRES_DATABASE=thymeleafdemonstration
POSTGRES_USER=postgres
POSTGRES_PASSWORD=secret
----
