package com.thymeleaf.jfall.thymeleafdemonstration.data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@Table(name = "employee_entity")
public class Employee {

    @Id
    private Long personnelNumber;

    private String firstName;

    private String lastName;

    private String phoneNumber;

    private String email;

}
