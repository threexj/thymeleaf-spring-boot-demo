package com.thymeleaf.jfall.thymeleafdemonstration.data;

import org.springframework.data.repository.CrudRepository;

public interface ReservationEntityRepository extends CrudRepository<Reservation, Long> {
}
