package com.thymeleaf.jfall.thymeleafdemonstration;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

    @GetMapping("login")
    public String getLogin(@AuthenticationPrincipal UserDetails user) {
        if (isLoggedIn(user)) {
            return "redirect:/";
        }
        return "login";
    }

    private boolean isLoggedIn(final UserDetails user) {
        return user != null;
    }
}
