package com.thymeleaf.jfall.thymeleafdemonstration.core;

import java.util.List;

import com.thymeleaf.jfall.thymeleafdemonstration.api.model.CreateReservationFormData;
import com.thymeleaf.jfall.thymeleafdemonstration.data.Reservation;

public interface ReservationService {

    public List<Reservation> getAllReservations();

    public void saveReservation(final CreateReservationFormData formData);
}
