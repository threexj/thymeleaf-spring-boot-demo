package com.thymeleaf.jfall.thymeleafdemonstration.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.thymeleaf.jfall.thymeleafdemonstration.api.model.CreateReservationFormData;
import com.thymeleaf.jfall.thymeleafdemonstration.core.mapper.ToReservationEntityMapper;
import com.thymeleaf.jfall.thymeleafdemonstration.data.Employee;
import com.thymeleaf.jfall.thymeleafdemonstration.data.EmployeeEntityRepository;
import com.thymeleaf.jfall.thymeleafdemonstration.data.Reservation;
import com.thymeleaf.jfall.thymeleafdemonstration.data.ReservationEntityRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class ReservationServiceImpl implements ReservationService{

    private final ReservationEntityRepository repository;

    private final ToReservationEntityMapper toReservationEntityMapper;

    private final EmployeeEntityRepository employeeEntityRepository;

    public List<Reservation> getAllReservations() {
        final List<Reservation> reservations = new ArrayList<>();
        repository.findAll().forEach(reservations::add);
        return reservations;
    }

    public void saveReservation(final CreateReservationFormData formData) {
        final Long personnelNumber = formData.getPersonnelNumber();
        final Employee employee = getCurrentEmployee(personnelNumber).orElseThrow();
        final Reservation entity = toReservationEntityMapper.map(formData, employee);
        repository.save(entity);
    }

    private Optional<Employee> getCurrentEmployee(Long personnelNumber) {
        return employeeEntityRepository.findById(personnelNumber);
    }

}
