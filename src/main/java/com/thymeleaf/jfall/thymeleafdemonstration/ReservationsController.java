package com.thymeleaf.jfall.thymeleafdemonstration;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.thymeleaf.jfall.thymeleafdemonstration.api.model.CreateReservationFormData;
import com.thymeleaf.jfall.thymeleafdemonstration.core.ReservationService;
import com.thymeleaf.jfall.thymeleafdemonstration.data.Reservation;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
@RequestMapping("reservations")
public class ReservationsController {

    private final ReservationService reservationService;

    @GetMapping
    public String getReservations(final Model model) {
        final List<Reservation> allReservations = reservationService.getAllReservations();
        model.addAttribute("reservations", allReservations);
        return "reservations/reservations";
    }

    @GetMapping("create")
    public String getCreateReservations(final Model model) {
        model.addAttribute("reservation", new CreateReservationFormData());
        return "reservations/reservations-create";
    }

    @PostMapping("create")
    public String createReservation(@Valid @ModelAttribute CreateReservationFormData formData) {
        reservationService.saveReservation(formData);
        return "redirect:/reservations";
    }
}
